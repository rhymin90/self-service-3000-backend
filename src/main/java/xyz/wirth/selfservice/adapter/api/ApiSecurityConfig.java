package xyz.wirth.selfservice.adapter.api;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import java.util.List;

@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class ApiSecurityConfig {

  private static final String FRONTEND_LOCALHOST = "http://localhost:3000";
  private static final String SWAGGER_LOCALHOST = "http://localhost:8080";

  @Bean
  public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
    http.csrf()
        .disable()
        .cors()
        .configurationSource(corsConfiguration())
        .and()
        .authorizeExchange()
        .pathMatchers(HttpMethod.GET, "/api/**")
        .permitAll()
        .pathMatchers(HttpMethod.GET, "/swagger-ui")
        .permitAll()
        .pathMatchers(HttpMethod.GET, "/webjars/**")
        .permitAll()
        .pathMatchers(HttpMethod.GET, "/v3/api-docs/**")
        .permitAll();
    return http.build();
  }

  @Bean
  CorsConfigurationSource corsConfiguration() {
    CorsConfiguration corsConfig = new CorsConfiguration();
    corsConfig.applyPermitDefaultValues();
    corsConfig.setAllowedOrigins(List.of(FRONTEND_LOCALHOST, SWAGGER_LOCALHOST));

    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", corsConfig);
    return source;
  }
}
