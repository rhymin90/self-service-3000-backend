package xyz.wirth.selfservice.adapter.api.v1;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import xyz.wirth.selfservice.ports.StagePersistence;

import java.util.Collections;

@Log4j2
@Component
@AllArgsConstructor
public class ApiHandlerV1 {

  private final StagePersistence stagePersistence;

  @NonNull
  public Mono<ServerResponse> getAllStages(ServerRequest request) {
    log.info("Request: {} {}", request.methodName(), request.path());

    var data = stagePersistence.getAllStages();

    return ServerResponse.ok()
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(data));
  }

  @NonNull
  public Mono<ServerResponse> getAllIssues(ServerRequest request) {
    log.info("Request: {} {}", request.methodName(), request.path());

    return ServerResponse.ok()
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(Collections.emptyList()));
  }
}
