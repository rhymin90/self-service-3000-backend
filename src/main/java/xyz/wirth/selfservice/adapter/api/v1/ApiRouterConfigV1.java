package xyz.wirth.selfservice.adapter.api.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.core.annotations.RouterOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicate;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import xyz.wirth.selfservice.domain.model.Stage;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration(proxyBeanMethods = false)
public class ApiRouterConfigV1 {

  private static final String BASE_PATH = "/api/v1";

  @Bean
  @RouterOperation(
      operation =
          @Operation(
              operationId = "getAllStages",
              summary = "Find purchase order by ID",
              responses = {
                @ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content =
                        @Content(
                            array = @ArraySchema(schema = @Schema(implementation = Stage.class)))),
              }))
  public RouterFunction<ServerResponse> routes(ApiHandlerV1 apiHandlerV1) {
    return route(GET(BASE_PATH + "/stages").and(acceptJson()), apiHandlerV1::getAllStages)
        .andRoute(GET(BASE_PATH + "/issues").and(acceptJson()), apiHandlerV1::getAllStages);
  }

  private RequestPredicate acceptJson() {
    return accept(MediaType.APPLICATION_JSON);
  }
}
