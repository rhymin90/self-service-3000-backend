package xyz.wirth.selfservice.adapter.code;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import xyz.wirth.selfservice.ports.CodeRepository;

@Log4j2
@AllArgsConstructor
@Component
@Lazy
public class GitlabClient implements CodeRepository {}
