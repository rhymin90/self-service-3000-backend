package xyz.wirth.selfservice.adapter.persistence;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.wirth.selfservice.adapter.persistence.model.ContainerForDatabase;

import java.util.List;

@Repository
@Lazy
public interface ContainerRepository extends CrudRepository<ContainerForDatabase, String> {

  @Override
  List<ContainerForDatabase> findAll();
}
