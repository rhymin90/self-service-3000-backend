package xyz.wirth.selfservice.adapter.persistence;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import xyz.wirth.selfservice.adapter.persistence.mapper.DatabaseMapper;
import xyz.wirth.selfservice.domain.model.Container;
import xyz.wirth.selfservice.domain.model.Stage;
import xyz.wirth.selfservice.ports.StagePersistence;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.List;

@Log4j2
@AllArgsConstructor
@Component
@Lazy
public class StagePersistenceImpl implements StagePersistence {

  private final ContainerRepository repository;

  @PostConstruct
  private void setup() {
    var mockData = getMockData();
    getAllStages().forEach(it -> log.debug("Stage in DB on startup: {}", it));
    mockData.forEach(this::saveStage);
  }

  private List<Stage> getMockData() {
    return List.of(
        Stage.builder()
            .name("TEST")
            .timestamp(Instant.now())
            .container(
                List.of(
                    new Container("cool-batch", "1.0.0-333"),
                    new Container("cool-service", "2.0.0-222")))
            .build(),
        Stage.builder()
            .name("INT")
            .timestamp(Instant.now())
            .container(
                List.of(
                    new Container("cool-batch", "1.0.0-222"),
                    new Container("cool-service", "2.0.0-111")))
            .build(),
        Stage.builder()
            .name("PROD")
            .timestamp(Instant.now())
            .container(
                List.of(
                    new Container("cool-batch", "1.0.0-111"),
                    new Container("cool-service", "2.0.0-111")))
            .build());
  }

  @Override
  public List<Stage> getAllStages() {
    val container = repository.findAll();
    return DatabaseMapper.INSTANCE.map(container);
  }

  @Override
  public void saveStage(Stage stage) {
    val containerForDatabaseList = DatabaseMapper.INSTANCE.map(stage);
    repository.saveAll(containerForDatabaseList);
  }
}
