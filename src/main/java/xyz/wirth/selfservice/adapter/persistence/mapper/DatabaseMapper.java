package xyz.wirth.selfservice.adapter.persistence.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import xyz.wirth.selfservice.adapter.persistence.model.ContainerForDatabase;
import xyz.wirth.selfservice.domain.model.Container;
import xyz.wirth.selfservice.domain.model.Stage;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR, unmappedSourcePolicy = ReportingPolicy.WARN)
public abstract class DatabaseMapper {

  public static final DatabaseMapper INSTANCE = Mappers.getMapper(DatabaseMapper.class);

  public List<ContainerForDatabase> map(Stage stage) {
    return stage.getContainer().stream()
        .map(
            it ->
                ContainerForDatabase.builder()
                    .name(it.getName())
                    .version(it.getVersion())
                    .timestamp(stage.getTimestamp())
                    .stage(stage.getName())
                    .build())
        .collect(Collectors.toList());
  }

  public abstract Container mapContainer(ContainerForDatabase containerForDatabase);

  public List<Stage> map(List<ContainerForDatabase> containerForDatabaseList) {
    var containerByStage =
        containerForDatabaseList.stream()
            .collect(Collectors.groupingBy(ContainerForDatabase::getStage));

    return containerByStage.entrySet().stream()
        .map(
            it ->
                Stage.builder()
                    .timestamp(it.getValue().get(0).getTimestamp()) // FIXME
                    .name(it.getKey())
                    .container(
                        it.getValue().stream().map(this::mapContainer).collect(Collectors.toList()))
                    .build())
        .collect(Collectors.toList());
  }
}
