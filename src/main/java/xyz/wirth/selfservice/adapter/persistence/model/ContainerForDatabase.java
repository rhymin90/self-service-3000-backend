package xyz.wirth.selfservice.adapter.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@IdClass(ContainerForDatabasePk.class)
public class ContainerForDatabase {
  @Id private String name;
  @Id private String stage;
  private Instant timestamp;
  private String version;
}
