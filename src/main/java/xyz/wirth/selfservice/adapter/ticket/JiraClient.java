package xyz.wirth.selfservice.adapter.ticket;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import xyz.wirth.selfservice.domain.model.Issue;
import xyz.wirth.selfservice.ports.TicketSystem;

@Log4j2
@AllArgsConstructor
@Component
@Lazy
public class JiraClient implements TicketSystem {

  @Override
  public Issue getIssueForKey(String key) {
    throw new UnsupportedOperationException("Method not implemented!");
  }
}
