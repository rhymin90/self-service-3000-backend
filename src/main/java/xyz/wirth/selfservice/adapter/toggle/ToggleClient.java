package xyz.wirth.selfservice.adapter.toggle;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import xyz.wirth.selfservice.domain.model.Tenant;
import xyz.wirth.selfservice.domain.model.Toggle;
import xyz.wirth.selfservice.ports.ToggleReader;

import java.util.Collection;
import java.util.Map;

@Log4j2
@AllArgsConstructor
@Component
@Lazy
public class ToggleClient implements ToggleReader {

  @Override
  public Map<Tenant, Collection<Toggle>> getTogglesForStage(String stage) {
    throw new UnsupportedOperationException("Method not implemented!");
  }
}
