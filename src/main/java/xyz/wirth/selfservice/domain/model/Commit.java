package xyz.wirth.selfservice.domain.model;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.regex.Pattern;

@Value
@Builder
public class Commit {
  public static final Pattern TICKET_KEY_REGEX = Pattern.compile("[AZ]{3}-d+");

  String hash;
  String message;
  LocalDateTime timestamp;

  public Optional<String> extractTicket() {
    var matcher = TICKET_KEY_REGEX.matcher(message);
    if (matcher.find()) {
      return Optional.of(matcher.group(0));
    }
    return Optional.empty();
  }
}
