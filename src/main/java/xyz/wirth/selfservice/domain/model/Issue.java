package xyz.wirth.selfservice.domain.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Issue {}
