package xyz.wirth.selfservice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Stage {

  private String name;
  private Instant timestamp;
  @Builder.Default private List<Container> container = Collections.emptyList();
}
