package xyz.wirth.selfservice.domain.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Toggle {
  private final String name;
  private final String reference;
  private final boolean enabled;
}
