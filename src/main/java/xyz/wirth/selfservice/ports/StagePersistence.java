package xyz.wirth.selfservice.ports;

import xyz.wirth.selfservice.domain.model.Stage;

import java.util.Collection;

public interface StagePersistence {

  Collection<Stage> getAllStages();

  void saveStage(Stage stage);
}
