package xyz.wirth.selfservice.ports;

import xyz.wirth.selfservice.domain.model.Issue;

public interface TicketSystem {

  Issue getIssueForKey(String key);
}
