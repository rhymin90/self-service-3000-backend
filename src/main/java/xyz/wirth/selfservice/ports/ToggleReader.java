package xyz.wirth.selfservice.ports;

import xyz.wirth.selfservice.domain.model.Tenant;
import xyz.wirth.selfservice.domain.model.Toggle;

import java.util.Collection;
import java.util.Map;

public interface ToggleReader {

  Map<Tenant, Collection<Toggle>> getTogglesForStage(String stage);
}
