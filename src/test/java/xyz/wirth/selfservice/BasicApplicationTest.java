package xyz.wirth.selfservice;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import xyz.wirth.selfservice.domain.model.Stage;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BasicApplicationTest {
  @LocalServerPort private int port;

  @Autowired private TestRestTemplate restTemplate;

  @Test
  void contextLoads() {
    // given
    var url = "http://localhost:" + port + "/api/v1/stages";

    // when
    val result = restTemplate.getForEntity(URI.create(url), Stage[].class);

    // then
    assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
    assertThat(result.getBody()).hasSize(3);
  }
}
